#!../../bin/linux-x86_64/KeithleyIOC

#- You may have to change Keithley to something else
#- everywhere it appears in this file

< envPaths


cd "${TOP}"

# For use as a generic docker iocBoot

epicsEnvSet("STREAM_PROTOCOL_PATH","$(KEITHLEY)/db")

## Register all support components
dbLoadDatabase "dbd/KeithleyIOC.dbd"
KeithleyIOC_registerRecordDeviceDriver pdbbase

drvAsynIPPortConfigure("KeithleyPort","$(IOC_IP):$(IOC_PORT)")
asynOctetSetInputEos("KeithleyPort", -1, "\n")
asynOctetSetOutputEos("KeithleyPort", -1, "\n")


set_requestfile_path("${TOP}/iocBoot/${IOC}", "")

set_savefile_path("/opt/epics/autosave")
set_pass0_restoreFile("settings.sav","PREFIX=$(IOC_PREFIX)")
save_restoreSet_DatedBackupFiles(0)

## Load record instances
dbLoadRecords("db/$(VARIANT).db","PREFIX=$(IOC_PREFIX),PORT=KeithleyPort,PROTO=Keithley$(VARIANT).proto")
dbLoadRecords("$(ASYN)/db/asynRecord.db","P=$(IOC_PREFIX):,R=asyn,PORT=KeithleyPort,ADDR=-1,IMAX=0,OMAX=0")


cd "${TOP}/iocBoot/${IOC}"
iocInit

create_monitor_set("settings.req",5, "PREFIX=$(IOC_PREFIX)")



