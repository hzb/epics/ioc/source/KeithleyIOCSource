### "Generic" IOC for Keithley Electrometer

This IOC expects to run in an environment where the following environment variables are set. The following are examples

    - IOC_SYS= SISSY2ES12X
    - IOC_DEV= Keithley01
    - IOC_PORT= 9001
    - IOC_IP= 192.168.169.76
    - VARIANT= 6514

Additionally, a folder should be available at `/opt/epics/autosave/$(IOC_SYS)_$(IOC_DEV)`. It's intended that this directory is mounted as a volume by something like docker-compose.

